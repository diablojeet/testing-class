package com.mercury.test;

import java.awt.AWTException;
import java.io.IOException;

import org.sikuli.script.FindFailed;

import com.mercury.test.MethodRepository;

public class DriverClass {

	/**
	 * @param args
	 * @throws InterruptedException  
	 * @throws AWTException 
	 * @throws IOException 
	 * @throws FindFailed 
	 */
	public static void main(String[] args) throws InterruptedException, AWTException, IOException, FindFailed {
		//MethodRepository Sel = new MethodRepository();
		//Sel.browserlaunch();
		
		//MethodRepository.winauth();
		
		MethodRepository.browserlaunch();
		MethodRepository.applaunch();
		MethodRepository.validlogin();
		Thread.sleep(3000);
		/*MethodRepository.verifyvalidlogin();
		MethodRepository.seltrip();
		MethodRepository.selpassengers();
		MethodRepository.seldepartcity();
		MethodRepository.seldate();
		MethodRepository.selarrival();
		MethodRepository.selclass();
		Thread.sleep(4000);
		MethodRepository.clickcontinue();
		Thread.sleep(4000);
		MethodRepository.browserclose();*/
}
}