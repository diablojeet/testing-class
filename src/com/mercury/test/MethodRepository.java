package com.mercury.test;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;

//import org.apache.bcel.generic.Select;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class MethodRepository {
	static WebDriver d1;
	public static void winauth() throws InterruptedException, IOException {
		System.setProperty("webdriver.chrome.driver", "E:\\Tools\\chromedriver.exe");
		d1 = new ChromeDriver();
		d1.manage().window().maximize();
		d1.get("https://upload.photobox.com/en/#computer");
		Thread.sleep(5000);
		WebElement browse = d1.findElement(By.xpath("//button[@id = 'button_desktop']"));
		Actions a1 = new Actions(d1);
		a1.moveToElement(browse).click().perform();
		Runtime.getRuntime().exec("E:\\AutoIT Script\\WinAuth.exe");
	}
	public static void browserlaunch() throws InterruptedException{
		System.setProperty("webdriver.chrome.driver", "E:\\Tools\\chromedriver.exe");
		d1 = new ChromeDriver();
		d1.manage().window().maximize();
	}
	public static void applaunch() throws InterruptedException{
		d1.get("http://newtours.demoaut.com/");
	}
	public static void validlogin() throws AWTException, FindFailed, InterruptedException{
		WebElement uName = d1.findElement(By.name("userName"));
		uName.sendKeys("dasd");
		WebElement pWord = d1.findElement(By.name("password"));
		pWord.sendKeys("dasd");
		//WebElement login = d1.findElement(By.name("login"));
		//WebElement login = d1.findElement(By.xpath("//input[@value = 'Login']"));
		//login.click();
		Thread.sleep(3000);
		Screen s1 = new Screen();
		Pattern p1 = new Pattern("E:\\Test Pics\\Sikulitest.JPG");
		s1.click(p1);
		/*Robot clklogin = new Robot();
		clklogin.keyPress(KeyEvent.VK_TAB);
		clklogin.keyRelease(KeyEvent.VK_TAB);
		clklogin.keyPress(KeyEvent.VK_ENTER);
		clklogin.keyRelease(KeyEvent.VK_ENTER);*/
	}
	public static void verifyvalidlogin(){
		String xpectedtitle = "Find a Flight: Mercury Tours:";
		String actualtitle = d1.getTitle();
		System.out.println(actualtitle);
		if (xpectedtitle.equalsIgnoreCase(actualtitle)){
			System.out.println("Test Passed");
		}
		else{
			System.out.println("Test Failed");
		}
	}
	public static void seltrip(){
		WebElement choosetrip = d1.findElement(By.xpath("//input[@value = 'oneway']"));
		Actions a1 = new Actions(d1);
		a1.moveToElement(choosetrip).click().perform();	
	}
	public static void selpassengers() {
		WebElement choosepass = d1.findElement(By.xpath("//select[@name = 'passCount']"));
		Select s1 = new Select(choosepass);
		s1.selectByValue("2");
	}
	public static void seldepartcity() {
		WebElement choosedepart = d1.findElement(By.xpath("//select[@name = 'fromPort']"));
		Select s1 = new Select(choosedepart);
		s1.selectByValue("New York");
	}
	public static void seldate() {
		WebElement choosemonth = d1.findElement(By.xpath("//select[@name = 'fromMonth']"));
		Select s1 = new Select(choosemonth);
		s1.selectByValue("5");
		WebElement choosedate = d1.findElement(By.xpath("//select[@name = 'fromDay']"));
		Select s2 = new Select(choosedate);
		s2.selectByValue("10");
	}
	public static void selarrival() {
		WebElement choosedepart = d1.findElement(By.xpath("//select[@name = 'toPort']"));
		Select s1 = new Select(choosedepart);
		s1.selectByValue("Sydney");
	}
	public static void selclass() {
		WebElement chooseclass = d1.findElement(By.xpath("//input[@value = 'Business']"));
		Actions a1 = new Actions(d1);
		a1.moveToElement(chooseclass).click().perform();
	}
	public static void clickcontinue() {
		WebElement contclk = d1.findElement(By.xpath("//input[@name = 'findFlights']"));
		Actions clk = new Actions(d1);
		clk.moveToElement(contclk).click().perform();
	}
	public static void browserclose(){
		d1.close();
	}
}